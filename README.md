# README #

Please read all of the following.

### What is this Framework for? ###

* This Framework is for the StowMarines mission makers
* [StowMarines Website](http://stowmarines.epizy.com/forum/)

### How do I get set up? ###

* Head over to the wiki to find out how to download and install the latest version of this Framework.

### Credits ###

* Bitbucket Managed by BJS
* Framework Created by BJS

### I have an issue, who do I talk to? ###

* Talk to a member of HR
* Report the issue in the issue tracker